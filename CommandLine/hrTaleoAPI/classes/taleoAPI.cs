﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using RestSharp;
using Newtonsoft.Json.Linq;
using hmeUtil;
using System.Data;
using System.Collections.Generic;
using System.Reflection;

namespace hrTaleoAPI.classes
{
    class taleoAPI
    {
        public loginResponse login()
        {
            loginResponse loginResponse = new loginResponse();
            JavaScriptSerializer json = new JavaScriptSerializer();
            //string loginCred = "orgCode=HME&userName=Maxx&password=thekooks1";
            ////byte[] bytes = Encoding.Default.GetBytes(loginCred);
            //byte[] bytes = Encoding.UTF8.GetBytes(loginCred);
            //string utf8_String = Encoding.UTF8.GetString(bytes);

            //logindto cred = new logindto();
            //cred.orgCode = "HME";
            //cred.userName = "Maxx";
            //cred.password = "Thekooks2!";
            //string jsonObj = json.Serialize(cred);
            //byte[] utf = Encoding.UTF8.GetBytes(jsonObj);

            //var data = Encoding.ASCII.GetBytes(loginCred);

            //var binaryString = ToBinary(ConvertToByteArray(loginCred, Encoding.UTF8));
            //var jsonbinaryString = ToBinary(ConvertToByteArray(jsonObj, Encoding.UTF8));

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;

            string url = GetURL("HME").response.URL + "login";
            loginResponse.url = GetURL("HME").response.URL;

            var client = new RestClient(url);
            var request = new RestRequest();

            request.Method = Method.POST;
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("orgCode", "HME", ParameterType.GetOrPost);
            request.AddParameter("userName", "Maxx", ParameterType.GetOrPost);
            request.AddParameter("password", "Thekooks2!", ParameterType.GetOrPost);

            var response = client.Execute(request);
            var content = response.Content; // raw content as string  
            tokendto tokendto = (tokendto)json.Deserialize<tokendto>(content);
            loginResponse.authToken = tokendto.response.authToken;
            return loginResponse;




            //var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            //httpWebRequest.ContentType = "application/json";
            //httpWebRequest.Method = "POST";
            //httpWebRequest.Accept = "application/json; charset=utf-8";
            ////httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            ////httpWebRequest.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";
            ////httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            //httpWebRequest.ContentLength = loginCred.Length;
            ////httpWebRequest.Headers.Add("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept");
            ////httpWebRequest.Headers.Add("Access-Control-Allow-Origin", "*");

            ////using (var stream = httpWebRequest.GetRequestStream())
            ////{
            ////    //stream.Write(utf, 0, utf.Length);
            ////}

            //using (var sw = new StreamWriter(httpWebRequest.GetRequestStream()))
            //{
            //    sw.Write(loginCred);
            //    //sw.Write(binaryString);
            //}
            //string xresponse = string.Empty;
            //HttpWebResponse login_resp = (HttpWebResponse)httpWebRequest.GetResponse();
            //using (var streamReader = new StreamReader(login_resp.GetResponseStream()))
            //{
            //    xresponse = streamReader.ReadToEnd();
            //}
        }

        private urldto GetURL(string company)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            var Request = (HttpWebRequest)WebRequest.Create("https://tbe.taleo.net/MANAGER/dispatcher/api/v1/serviceUrl/" + company);
            Request.ContentType = "application/json";
            Request.Method = "GET";
            string url_response = string.Empty;
            HttpWebResponse resp = (HttpWebResponse)Request.GetResponse();
            urldto urldto;
            using (var streamReader = new StreamReader(resp.GetResponseStream()))
            {
                url_response = streamReader.ReadToEnd();
                urldto = (urldto)json.Deserialize<urldto>(url_response);
            }
            return urldto;
        }

        public bool Logout(loginResponse loginResponse)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            var client = new RestClient(loginResponse.url + "logout");
            var request = new RestRequest();

            request.Method = Method.POST;
            request.AddHeader("Accept", "application/json");
            request.AddCookie("authToken", loginResponse.authToken);
            //request.Parameters.Clear();

            var response = client.Execute(request);
            var content = response.Content;
            tokendto tokendto = (tokendto)json.Deserialize<tokendto>(content);
            return tokendto.status.success == "True" ? true : false;
        }

        public GetEmployeeDTO GetEmployee(loginResponse loginResponse, string employee)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            var client = new RestClient(loginResponse.url + "object/employee/"+ employee);
            var request = new RestRequest();

            request.Method = Method.GET;
            request.AddHeader("Accept", "application/json");
            request.AddCookie("authToken", loginResponse.authToken);
            //request.Parameters.Clear();

            var response = client.Execute(request);
            string content = response.Content;
            string modifiedJson = ConvertEmployeeDTO(content);
            GetEmployeeDTO employeeJson = (GetEmployeeDTO)json.Deserialize<GetEmployeeDTO>(modifiedJson);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.certificate);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.comment);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.contactlog);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.department);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.division);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.education);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.historylog);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.location);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.manager);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.offBoardStatus);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.onBoardStatus);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.packets);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.reference);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.region);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.reportsIndirectly);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.residence);
            //RelationshipUrls(loginResponse, employeeJson.response.employee.relationshipUrls.workhistory);

            return employeeJson;
        }

        public EmployeeSearch SearchEmployee(loginResponse loginResponse, string filter)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            var client = new RestClient(loginResponse.url + "object/employee/search?"+ filter);
            var request = new RestRequest();

            request.Method = Method.GET;
            request.AddHeader("Accept", "application/json");
            request.AddCookie("authToken", loginResponse.authToken);
            //request.Parameters.Clear();

            var response = client.Execute(request);
            var content = response.Content;
            string modifiedJson = ConvertEmployeeDTO(content);

            EmployeeSearch employeeJson = (EmployeeSearch)json.Deserialize<EmployeeSearch>(modifiedJson);
            if (!employeeJson.status.success)
            {
                var detail = employeeJson.status.detail;
                if (detail.ToString() != "")
                {
                    //var errormessage = detail["errormessage"];
                }
            }
            else
            {
                foreach (var employee in employeeJson.response.searchResults)
                {
                    string company = employee.employee.Company;
                    //DateTime? rehireDate = employee.employee.RehireDate != null ? Convert.ToDateTime(employee.employee.RehireDate) : null;
                    DateTime? rehireDate = null;
                    if (employee.employee.RehireDate != null)
                        rehireDate = Convert.ToDateTime(employee.employee.RehireDate.ToString());
                    string creationDate = employee.employee.creationDate;
                }
                //Employee employee = employeeJson.response.searchResults
            }

 
            return employeeJson;
        }

        public UpdateEmployeeDTO UpdateEmployee(loginResponse loginResponse, string employee, string data)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            var client = new RestClient(loginResponse.url + "object/employee/" + employee);
            var request = new RestRequest();

            request.Method = Method.PUT;
            request.AddHeader("Accept", "application/json");
            request.AddCookie("authToken", loginResponse.authToken);
            //request.Parameters.Clear();
            //request.AddJsonBody(new { AssignedShift = "1st Shift" });
            //string data = "{ \"employee\":{ \"2020NewShiftDifferential\":\"2\",\"lastName\":\"Testing\",\"PayRate\":\"50\"}}";
            request.AddJsonBody(data);
            var response = client.Execute(request);
            string content = response.Content;
            UpdateEmployeeDTO jsonUpdateResponse = (UpdateEmployeeDTO)json.Deserialize<UpdateEmployeeDTO>(content);
            return jsonUpdateResponse;
        }

        public void RelationshipUrls(loginResponse loginResponse, string relationship)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            var client = new RestClient(relationship);
            var request = new RestRequest();

            request.Method = Method.GET;
            request.AddHeader("Accept", "application/json");
            request.AddCookie("authToken", loginResponse.authToken);
            var response = client.Execute(request);
            string content = response.Content;
            string modifiedJson = ConvertEmployeeDTO(content);
            //GetEmployeeDTO employeeJson = (GetEmployeeDTO)json.Deserialize<GetEmployeeDTO>(modifiedJson);
            //return employeeJson;
        }

        public void Requisition(loginResponse loginResponse, string requisition)
        {
 
            JavaScriptSerializer json = new JavaScriptSerializer();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            var client = new RestClient(loginResponse.url + "object/requisition/" + requisition);
            var request = new RestRequest();

            request.Method = Method.GET;
            request.AddHeader("Accept", "application/json");
            request.AddCookie("authToken", loginResponse.authToken);
            var response = client.Execute(request);
            string content = response.Content;
            //string modifiedJson = ConvertEmployeeDTO(content);
            GetRequisitionDTO requisitionJson = (GetRequisitionDTO)json.Deserialize<GetRequisitionDTO>(content);
            //return employeeJson;
        }

        public void UpdateRequisition(loginResponse loginResponse, string requisition)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            var client = new RestClient(loginResponse.url + "object/requisition/" + requisition);
            var request = new RestRequest();

            request.Method = Method.PUT;

            string data = "{ \"requisition\":{ \"reqId\":\"3750195\", \"PositionOfferedTo\":\"Sam Watkins\",\"FinalFilledDate\":\"2020-09-12T08:39PDT\"}}";
            //string data = "{ \"requisition\":{ \"reqId\":\"3750195\", \"PositionOfferedTo\":\"Sam Watkins\"}}";
            request.AddJsonBody(data);


            request.AddHeader("Accept", "application/json");
            request.AddCookie("authToken", loginResponse.authToken);
            var response = client.Execute(request);
            string content = response.Content;
            //string modifiedJson = ConvertEmployeeDTO(content);
            //GetEmployeeDTO employeeJson = (GetEmployeeDTO)json.Deserialize<GetEmployeeDTO>(modifiedJson);
            //return employeeJson;
        }


        public void CompanyGoals(loginResponse loginResponse, string url)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            var client = new RestClient(loginResponse.url+ @"object/" + url);
            var request = new RestRequest();

            request.Method = Method.GET;
            request.AddHeader("Accept", "application/json");
            request.AddCookie("authToken", loginResponse.authToken);
            var response = client.Execute(request);
            string content = response.Content;
            //string modifiedJson = ConvertEmployeeDTO(content);
            //GetEmployeeDTO employeeJson = (GetEmployeeDTO)json.Deserialize<GetEmployeeDTO>(modifiedJson);
            //return employeeJson;
        }

        public ExcelRecords ExcelImport(string excelfile)
        {
            xlsxReader xlsxReader = new xlsxReader();
            DataSet ds = new DataSet();
            ds = xlsxReader.readxlsx(excelfile);
            IList<PropertyInfo> propertiesList;
            propertiesList = typeof(ExcelRecord).GetProperties().ToList();
            ExcelRecord record = new ExcelRecord();
            ExcelRecords records = new ExcelRecords();
            records.Records = new List<ExcelRecord>();
            for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
            {
                record = new ExcelRecord();
                for (int k = 0; k < 4; k++)
                {
                    propertiesList[k].SetValue(record, ds.Tables[0].Rows[i].ItemArray[k].ToString());
                }
                records.Records.Add(record);
            }
            return records;
        }


        public ExcelRecords CheckForFiles(string root, string filter)
        {
            //string[] files;
            ExcelRecords records = new ExcelRecords();
            //C:\Dev\taleo
            if (!Directory.Exists(root))
            {
                //Directory.CreateDirectory(root);
            }

            //DirectoryInfo dirInfo = new DirectoryInfo(root);
            //FileSystemInfo[] allFiles = dirInfo.GetFileSystemInfos();
            //var orderedFiles = allFiles.OrderBy(f => f.CreationTime);

            DirectoryInfo di = new DirectoryInfo(root);
            FileInfo[] fiArray = di.GetFiles(filter);
            //bool status;
            if (fiArray.Length > 0)
            {
                //loginResponse loginResponse = login();
                Array.Sort(fiArray, (x, y) => StringComparer.OrdinalIgnoreCase.Compare(x.CreationTime, y.CreationTime));
                foreach (FileInfo fi in fiArray)
                {
                    records = ExcelImport(fi.FullName);
                    string destination = fi.FullName.Replace(root, root + @"\archive");
                    File.Copy(fi.FullName, destination, true);
                    File.Delete(fi.FullName);
                    break;
                }
                return records;
            }
            else
            {
                return null;
            }

            //files = Directory.GetFiles(root, "*.xlsx");
            //foreach (string _file in files)
            //{
            //    string destination = _file.Replace(root, root + @"\archive");
            //    //ExcelRecords records = ExcelImport(_file);
            //    //File.Copy(_file, destination, true);
            //    //File.Delete(_file);
            //}
        }

        private string ConvertEmployeeDTO(string content)
        {
            string modifiedJson = content.Replace("2020", "_2020");
            modifiedJson = modifiedJson.Replace("AccessCard#", "AccessCardNo");
            modifiedJson = modifiedJson.Replace("AlternateContact-City", "AlternateContactCity");
            modifiedJson = modifiedJson.Replace("AlternateContact-EmailAddress", "AlternateContactEmailAddress");
            modifiedJson = modifiedJson.Replace("AlternateContact-State", "AlternateContactState");
            modifiedJson = modifiedJson.Replace("AlternateContact-ZipCode", "AlternateContactZipCode");
            modifiedJson = modifiedJson.Replace("AlternateContactHomePhone#", "AlternateContactHomePhoneNo");
            modifiedJson = modifiedJson.Replace("AlternateContactMobile#", "AlternateContactMobileNo");
            modifiedJson = modifiedJson.Replace("RunReport:DirectReports", "RunReportDirectReports");
            modifiedJson = modifiedJson.Replace("Facilities&ActivitiesWaiverSigned", "FacilitiesActivitiesWaiverSigned");
            modifiedJson = modifiedJson.Replace("ListA-DocumentTitle", "ListADocumentTitle");
            modifiedJson = modifiedJson.Replace("ListA-ExpirationDate", "ListAExpirationDate");
            modifiedJson = modifiedJson.Replace("ListB-DocumentTitle", "ListBDocumentTitle");
            modifiedJson = modifiedJson.Replace("ListB-ExpirationDate", "ListBExpirationDate");
            modifiedJson = modifiedJson.Replace("ListC-DocumentTitle", "ListCDocumentTitle");
            modifiedJson = modifiedJson.Replace("ListC-ExpirationDate", "ListCExpirationDate");
            modifiedJson = modifiedJson.Replace("NewHireMgr-DirectReportList", "NewHireMgrDirectReportList");
            modifiedJson = modifiedJson.Replace("OvertimeRate(s)ofPay", "OvertimeRateofPay");
            modifiedJson = modifiedJson.Replace("PrimaryContact-City", "PrimaryContactCity");
            modifiedJson = modifiedJson.Replace("PrimaryContact-EmailAddress", "PrimaryContactEmailAddress");
            modifiedJson = modifiedJson.Replace("PrimaryContact-State", "PrimaryContactState");
            modifiedJson = modifiedJson.Replace("PrimaryContact-StreetAddress", "PrimaryContactStreetAddress");
            modifiedJson = modifiedJson.Replace("PrimaryContact-ZipCode", "PrimaryContactZipCode");
            modifiedJson = modifiedJson.Replace("PrimaryContactHomePhone#", "PrimaryContactHomePhoneNo");
            modifiedJson = modifiedJson.Replace("PrimaryContactMobilePhone#", "PrimaryContactMobilePhoneNo");
            modifiedJson = modifiedJson.Replace("WorkAuthorization(I-9)", "WorkAuthorizationI9");
            return modifiedJson;
        }

        public static byte[] ConvertToByteArray(string str, Encoding encoding)
        {
            return encoding.GetBytes(str);
        }


        public static String ToBinary(Byte[] data)
        {
            return string.Join(" ", data.Select(byt => Convert.ToString(byt, 2).PadLeft(8, '0')));
        }
    }
    public class logindto
    {
        public string orgCode { get; set; }
        public string userName { get; set; }
        public string password { get; set; }

    }


    public class urldto
    {
        public urlrequest response { get; set; }
        public callstatus status { get; set; }
    }

    public class tokendto
    {
        public tokenrequest response { get; set; }
        public callstatus status { get; set; }
    }
    public class tokenrequest
    {
        public string authToken { get; set; }
        //public callstatus status { get; set; }

    }
    public class urlrequest
    {
        public string URL { get; set; }
        //public callstatus status { get; set; }

    }
    public class callstatus
    {
        public string success { get; set; }
        public Detail detail { get; set; }
    }

    public class loginResponse
    {
        public string authToken { get; set; }
        public string url { get; set; }
    }

    public class ExcelRecords
    {
        public List<ExcelRecord> Records { get; set; }
    }

    public class ExcelRecord
    {
        public string EE_ID { get; set; }
        public string Last_Name { get; set; }
        public string Regular_Pay_Rate { get; set; }
        public string Shift_Differential { get; set; }
    }
}
