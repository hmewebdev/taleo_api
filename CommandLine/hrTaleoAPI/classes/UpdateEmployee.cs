﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hrTaleoAPI.classes
{
    class UpdateEmployee
    {
        public void UpdateRecord(string path, string filter)
        {
            bool status;
            loginResponse loginResponse;
            EmployeeSearch EmployeeSearch;
            GetEmployeeDTO GetEmployeeDTO;
            ExcelRecords records;
            taleoAPI api = new taleoAPI();
            loginResponse = api.login();
            records = api.CheckForFiles(path, filter);
            while(records != null){
                foreach (ExcelRecord record in records.Records)
                {
                    EmployeeSearch = api.SearchEmployee(loginResponse, "employeeNumber="+ record.EE_ID);
                    if (EmployeeSearch.status.success)
                    {
                        GetEmployeeDTO = api.GetEmployee(loginResponse, EmployeeSearch.response.searchResults[0].employee.employeeId.ToString());
                        string data = "{ \"employee\":{ \"Rate2\":\"" + record.Shift_Differential + "\",\"lastName\":\"" + record.Last_Name + "\",\"PayRate\":\"" + record.Regular_Pay_Rate + "\"}}";
                        UpdateEmployeeDTO jsonUpdateResponse = api.UpdateEmployee(loginResponse, EmployeeSearch.response.searchResults[0].employee.employeeId.ToString(), data);
                    }
                }
                records = api.CheckForFiles(path, filter);
            }
            status = api.Logout(loginResponse);

        }
    }
}
