﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hrTaleoAPI.classes
{
    class taleoEmployees
    {
    }
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Pagination
    {
        public string next { get; set; }
        public int total { get; set; }
        public string self { get; set; }
    }

    public class RelationshipUrls
    {
        public string department { get; set; }
        public string division { get; set; }
        public string reportsIndirectly { get; set; }
        public string location { get; set; }
        public string manager { get; set; }
        public string offBoardStatus { get; set; }
        public string onBoardStatus { get; set; }
        public string region { get; set; }
        public string status { get; set; }
        public string workhistory { get; set; }
        public string reference { get; set; }
        public string education { get; set; }
        public string residence { get; set; }
        public string certificate { get; set; }
        public string packets { get; set; }
        public string comment { get; set; }
        public string historylog { get; set; }
        public string contactlog { get; set; }
    }

    public class Employee
    {
        public object _2020EffectiveDate { get; set; }
        public string _2020NewCompensation { get; set; }
        public string _2020NewOvertimeRate { get; set; }
        public string _2020NewRegularpayRate { get; set; }
        public string _2020NewShiftDifferential { get; set; }
        public string _2020NewTitle { get; set; }
        public string _2020PercentIncrease { get; set; }
        public object emplAcceptedOfferId { get; set; }
        public string AccessCardNo { get; set; }
        public string creationDate { get; set; }
        public string address { get; set; }
        public string AgencyRate { get; set; }
        public string AlternateContactCity { get; set; }
        public string AlternateContactEmailAddress { get; set; }
        public string AlternateContactState { get; set; }
        public string AlternateContactAddress { get; set; }
        public string AlternateContactZipCode { get; set; }
        public string AlternateContactHomePhoneNo { get; set; }
        public string AlternateContactMobileNo { get; set; }
        public string AlternateContactName { get; set; }
        public string AlternateContactRelationship { get; set; }
        public string AreaCode { get; set; }
        public string AreaCodeHome { get; set; }
        public string areasOfExpertise { get; set; }
        public string AssignedShift { get; set; }
        public string BenefitsElig { get; set; }
        public object candidate { get; set; }
        public string careerAmbitions { get; set; }
        public string city { get; set; }
        public string EligibleforCommission { get; set; }
        public string Company { get; set; }
        public string country { get; set; }
        public string county { get; set; }
        public string criticalExperience { get; set; }
        public bool criticalPosition { get; set; }
        public string birthdate { get; set; }
        public int department { get; set; }
        public string desiredAdvancement { get; set; }
        public List<object> developmentPath { get; set; }
        public string RunReportDirectReports { get; set; }
        public List<int> division { get; set; }
        public string employeeNumber { get; set; }
        public int numberOfReports { get; set; }
        public int numberOfSubordinates { get; set; }
        public bool emplIsActiveStatus { get; set; }
        public string lastReviewsScoreAvg { get; set; }
        public object lastReviewManager { get; set; }
        public string lastReviewScore { get; set; }
        public bool lockedFromEws { get; set; }
        public bool emplUserIsReviewMgr { get; set; }
        public string ewsLogin { get; set; }
        public string ewsPassword { get; set; }
        public string duration { get; set; }
        public object emplPosEndDate { get; set; }
        public string FLSADescription { get; set; }
        public bool FacilitiesActivitiesWaiverSigned { get; set; }
        public string flightRiskReason { get; set; }
        public bool flightRisk { get; set; }
        public string gender { get; set; }
        public bool highPerformer { get; set; }
        public string highestEducationLev { get; set; }
        public string hiredDate { get; set; }
        public object hiredForReqId { get; set; }
        public string hiredForReqJobCode { get; set; }
        public string hiredForReqTitle { get; set; }
        public string HourlyPayRate { get; set; }
        public string hourlyWageOld { get; set; }
        public int employeeId { get; set; }
        public string ITSetupNeeds { get; set; }
        public List<object> reportsIndirectly { get; set; }
        public string IndividualWithDisability { get; set; }
        public bool activeEmplPosition { get; set; }
        public string jobClassification { get; set; }
        public string jobCode { get; set; }
        public string jobTitle { get; set; }
        public List<object> languagesSpoken { get; set; }
        public string lastDayDate { get; set; }
        public string lastUpdated { get; set; }
        public List<object> leadershipQual { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string Middlename { get; set; }
        public string licenseNumber { get; set; }
        public string ListADocumentTitle { get; set; }
        public object ListAExpirationDate { get; set; }
        public string ListBDocumentTitle { get; set; }
        public object ListBExpirationDate { get; set; }
        public string ListCDocumentTitle { get; set; }
        public object ListCExpirationDate { get; set; }
        public int location { get; set; }
        public bool ssoLoginOnly { get; set; }
        public int? manager { get; set; }
        public string ManagerTalentCenterAccess { get; set; }
        public string maritalStatus { get; set; }
        public string MeritAcknowledgementSignedoffInPers { get; set; }
        public string MeritIncreaseYear { get; set; }
        public string middleInitial { get; set; }
        public bool ModelReleaseFormApproved { get; set; }
        public string NewHireMgrDirectReportList { get; set; }
        public string nextLevelPosRank { get; set; }
        public int offBoardStatus { get; set; }
        public string OfferAmount { get; set; }
        public int onBoardStatus { get; set; }
        public bool emplEmailRequired { get; set; }
        public string OverallPerformance { get; set; }
        public string OvertimeRateofPay { get; set; }
        public string passportNumber { get; set; }
        public string payFrequency { get; set; }
        public string Personalemail { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneNumberHome { get; set; }
        public string EMPL__003 { get; set; }
        public string position { get; set; }
        public string potential { get; set; }
        public object preferredLocale { get; set; }
        public List<object> preferredLocations { get; set; }
        public string preferredTalentCenter { get; set; }
        public string Preferredname { get; set; }
        public string PrimaryContactCity { get; set; }
        public string PrimaryContactEmailAddress { get; set; }
        public string PrimaryContactState { get; set; }
        public string PrimaryContactStreetAddress { get; set; }
        public string PrimaryContactZipCode { get; set; }
        public string PrimaryContactHomePhoneNo { get; set; }
        public string PrimaryContactMobilePhoneNo { get; set; }
        public string PrimaryContactName { get; set; }
        public string PrimaryContactRelationship { get; set; }
        public string phone { get; set; }
        public object employeePicture { get; set; }
        public string promotionReadiness { get; set; }
        public string race { get; set; }
        public List<object> region { get; set; }
        public string PayRate { get; set; }
        public string RehireDate { get; set; }
        public string religion { get; set; }
        public string Remote { get; set; }
        public string resignedDate { get; set; }
        public object reviewApprover { get; set; }
        public object reviewManager { get; set; }
        public string reviewTemplate { get; set; }
        public string SAMAccountID { get; set; }
        public string salary { get; set; }
        public string salaryGrade { get; set; }
        public string salutation { get; set; }
        public string cellPhone { get; set; }
        public string Rate2 { get; set; }
        public string ssn { get; set; }
        public string startDate { get; set; }
        public object emplPosStartDate { get; set; }
        public string Stateprovinceterritory { get; set; }
        public string state { get; set; }
        public int status { get; set; }
        public string stockOptions { get; set; }
        public string address2 { get; set; }
        public string nameSuffix { get; set; }
        public string TaxIDType { get; set; }
        public string TempP { get; set; }
        public string TempNotes { get; set; }
        public object TempScheduledTermDate { get; set; }
        public object TempStartDate { get; set; }
        public object TempTermDate { get; set; }
        public string TempTermReason { get; set; }
        public string TerminationReason { get; set; }
        public string EMPL_WORK { get; set; }
        public List<object> veteran { get; set; }
        public string willingnessToRel { get; set; }
        public string WorkAuthorizationI9 { get; set; }
        public string WorkAuthorizationExpirationDate { get; set; }
        public string WorkEmail { get; set; }
        public string zipCode { get; set; }
        public RelationshipUrls relationshipUrls { get; set; }
    }

    public class SearchResult
    {
        public Employee employee { get; set; }
    }

    public class Response
    {
        public Pagination pagination { get; set; }
        public List<SearchResult> searchResults { get; set; }
    }

    public class GetEmployeeResponse
    {
        public Employee employee { get; set; }
    }

    public class Detail
    {
        public string errormessage { get; set; }
        public string error { get; set; }
        public string operation { get; set; }
        public string errorcode { get; set; }
    }

    public class Status
    {
        public bool success { get; set; }
        public Detail detail { get; set; }
    }

    public class EmployeeSearch
    {
        public Response response { get; set; }
        public Status status { get; set; }
    }

    public class GetEmployeeDTO
    {
        public GetEmployeeResponse response { get; set; }
        public Status status { get; set; }
    }

    public class UpdateEmployeeDTO
    {
        public UpdatemployeeResponse response { get; set; }
        public Status status { get; set; }
    }
    public class UpdatemployeeResponse
    {
        public string employeeid { get; set; }
        //public string object { get; set; }
    }

    public class PutEmployeeDTO
    {
        public Putemployee employee { get; set; }
    }
    public class Putemployee
    {
        public string PayRate { get; set; }
        public string AssignedShift { get; set; }
        public string _2020NewRegularpayRate { get; set; }
    }
}
