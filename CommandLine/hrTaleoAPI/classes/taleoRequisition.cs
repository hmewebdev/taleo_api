﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hrTaleoAPI.classes
{
    class taleoRequisition
    {
    }
    public class RequisitionRelationshipUrls
    {
        public string location { get; set; }
        public string status { get; set; }
        public string owners { get; set; }
        public string approvers { get; set; }
        public string offerApprs { get; set; }
        public string department { get; set; }
        public string region { get; set; }
        public string division { get; set; }
        public string reqRecruiter { get; set; }
        public string reqHiringManager { get; set; }
        public string attachments { get; set; }
        public string candidate { get; set; }
        public string expense { get; set; }
        public string comment { get; set; }
        public string questions { get; set; }
        public string approval { get; set; }
        public string historylog { get; set; }
        public string contactlog { get; set; }
        public string poster { get; set; }
    }

    public class Requisition
    {
        public int reqId { get; set; }
        public string InternalReqID { get; set; }
        public string title { get; set; }
        public int location { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string duration { get; set; }
        public string payRange { get; set; }
        public int numOpen { get; set; }
        public int numCands { get; set; }
        public string openedDate { get; set; }
        public object FinalFilledDate { get; set; }
        public string jobCat { get; set; }
        public int status { get; set; }
        public string creationDate { get; set; }
        public string lastUpdated { get; set; }
        public List<int> owners { get; set; }
        public List<int> approvers { get; set; }
        public List<int> offerApprs { get; set; }
        //public object description { get; set; }
        public string ReasonOpen { get; set; }
        public string IfReplacement_ForWho { get; set; }
        public string BusinessUnit { get; set; }
        //public string Department { get; set; }
        public string JobType { get; set; }
        public string JobFunction { get; set; }
        public string JobIndustry { get; set; }
        public int department { get; set; }
        public int region { get; set; }
        public int workflowDefId { get; set; }


        //public string zipCode { get; set; }
        //public bool isTemplate { get; set; }
        //public string eQuestPostingSite { get; set; }
        //public int division { get; set; }
        //public string inStatusDate { get; set; }
        //public bool isHotJob { get; set; }


        //public string REQUISITION_JT_FUNCTION { get; set; }
        //public string REQUISITION_JT_EDUCATION { get; set; }
        //public string jobBrief { get; set; }
        //public List<object> reqRecruiter { get; set; }
        //public List<int> reqHiringManager { get; set; }
        //public object DateofOfferasaTemp { get; set; }
        //public string HiringManager { get; set; }
        //public object DateReqSent { get; set; }
        //public object DateReqReOpened { get; set; }
        //public object AnticipatedStartDate { get; set; }
        //public string JobCategoryInternal { get; set; }//JobCategory(Internal)
        //public string HireType { get; set; }
        //public string Shift { get; set; }
        //public string IfAgencyName { get; set; }//IfAgency, Name:
        //public object DateReqHold { get; set; }
        //public string TempLoadedRate { get; set; }//TempLoadedRate:
        //public string TempPayRate { get; set; }//TempPayRate:
        //public string TempShiftDifferential { get; set; } //TempShiftDifferential:
        //public string TempTotalPayRate { get; set; }
        //public string PositionOfferedTo { get; set; }
        //public object DateOfferMade { get; set; }
        //public object AutoReqID { get; set; }
        //public object ReqNotes { get; set; }
        //public string JobCode { get; set; }
        //public string OfferAmount { get; set; } //OfferAmount:
        //public string HeadcountPlanYear { get; set; }
        //public string BenefitsEligibilityClass { get; set; }
        //public string Remote { get; set; }
        //public string JobDescription { get; set; }
        //public string ReasonForOpening { get; set; }
        //public string _25thPercentile { get; set; } //25thPercentile
        //public string _50thPercentile { get; set; } //50thPercentile
        //public string _75thPercentile { get; set; } //75thPercentile
        //public string Currency { get; set; }
        //public string IntakeMeetingPending { get; set; }
        //public string InterviewPanelKickOffScheduled { get; set; } //InterviewPanelKick-OffScheduled
        //public string ReqNotesHRONLY { get; set; } //ReqNotes-HRONLY
        //public string InterviewTeam { get; set; }
        //public string mgrreasonforopening { get; set; }
        //public string REQU__002 { get; set; }
        //public string CompensationAnalysis { get; set; }
        //public string TelecommuteEligible { get; set; } //TelecommuteEligible?
        //public string Commission { get; set; }
        //public string reqKeywords { get; set; }
        //public string COVIDCriticalPosition { get; set; } //COVID-CriticalPosition
        //public string WhichEmailDistributionListsshouldyo { get; set; }
        //public string IsthereaneedforaPCSetup { get; set; } //IsthereaneedforaPCSetup?
        //public string OtherSpecificSetupNeed { get; set; }
        //public string Officelicense { get; set; }
        //public string DeskPhone { get; set; }
        //public bool SoftPhone { get; set; }
        //public bool Monitor { get; set; }
        //public bool KeyboardMouse { get; set; } //Keyboard/Mouse
        //public bool DockingStation { get; set; }
        //public bool AXAccount { get; set; } //AXAccount?
        //public string HiringManagersPreferences { get; set; } //HiringManager'sPreferences
        //public List<object> Assessments { get; set; }
        //public string Dealbreaker { get; set; }
        //public string AdditionalIntakeNotes { get; set; }
        //public RequisitionRelationshipUrls relationshipUrls { get; set; }
        //public bool reqSetHiredDate { get; set; }
        //public bool reqDecrementOpenings { get; set; }
        //public bool reqEmailNonHired { get; set; }
        //public bool reqChangeStatusNonHired { get; set; }
        //public bool reqChangeStatusPosting { get; set; }
    }

    public class RequisitionResponse
    {
        public Requisition requisition { get; set; }
    }

    public class GetRequisitionDTO
    {
        public RequisitionResponse response { get; set; }
        public Status status { get; set; }
    }
}
